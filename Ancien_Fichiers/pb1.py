import os
from utils import *
import itertools
import sys 
import copy
import operator

def borne(code, n=10):
    """applique le modulo n à une roue"""
    return list(map(lambda x: x%n, code))

def tourner_roue(cadenas, roue, valeur=1, n=10):
    """fait tourner la roue d'un cadenas de value cran"""
    nouveau_cadenas = cadenas.copy()
    try:
        nouveau_cadenas[roue] += valeur
    except:
        print(roue, valeur, nouveau_cadenas)
        print(1/0)
    return borne(nouveau_cadenas, n=10)

def DFS(r=2,n=10,show_stack=False,frequence=500000, resume=False):
    """"renvoie une suite de coup permetant de passer par tout les cadenas"""
    try:
        #---------------------------------------------init---------------------------------------------
        cadenas_init = [0 for _ in range(r)]
        stack_roue = [
            0   #cadenas, derniere roue tournée
        ]
        stack_fait = [cadenas_init]
        ctr = 0
        ctr_init = 0
        max_len = 0

        #---------------------------------------------load config---------------------------------------------
        if resume:
            stack_fait, stack_roue = load_at("DFS_stack_fait"), load_at("DFS_stack_roue")
            ctr_init = load_at("DFS_ctr")
            ctr += ctr_init
            max_len = len("DFS_max_stack_roue")

        #---------------------------------------------run---------------------------------------------
        while stack_roue:
            #---------------------------------------------debug---------------------------------------------
            if ctr%frequence==0:
                if ctr%(frequence*20)==0:
                    print()
                    print('{:>12} | {:>12} | {:>12} | {:>9} | {:>9} | {} | {}'.format('ctr_init','ctr_total','faitdepuis','lenstack','max_len','20 premieres roues ',"stack"))
                s = stack_fait if show_stack else "X"
                print('{:>12} | {:>12} | {:>12} | {:>9} | {:>9} | {} | {}'.format(ctr_init, ctr, ctr-ctr_init, len(stack_roue),max_len, " ".join(map(str, stack_roue[:20])), s))
                save_at("DFS_stack_roue", stack_roue)
                save_at("DFS_stack_fait", stack_fait)
            ctr += 1

            #---------------------------------------------fin---------------------------------------------
            if len(stack_fait) == n**r :
                print("fin trouvé ! stack fait :\n",stack_fait)
                print("END")
                return stack_fait

            roue = stack_roue[-1]
            derniere_roue = stack_roue[-2] if len(stack_roue) >= 2 else -2 
            cadenas = stack_fait[-1]

            stack_roue[-1] += 1 #ajout roue

            #---------------------------------------------backtrap---------------------------------------------
            if roue == r:
                if len(stack_roue) > max_len:   #svg
                    max_len = len(stack_roue)
                    save_at("DFS_max_stack_roue", stack_roue)
                    save_at("DFS_max_stack_fait", stack_fait)
                stack_roue.pop()                #pop combi
                stack_fait.pop()
                continue

            #---------------------------------------------nouvelles combis---------------------------------------------
            if roue == derniere_roue-1:                                 #on ne reprend pas la même roue
                continue                                                

            nouveau_cadenas = borne(tourner_roue(cadenas, roue), n=n)   #nouvelle combi

            if not nouveau_cadenas in stack_fait:                       #pas déjà  fait
                stack_roue.append(0)
                stack_fait.append(nouveau_cadenas)


        #---------------------------------------------fin---------------------------------------------
        print("fin sans combinaison :'(")
        print("Meilleur résultat :\n",load_at("DFS_max_stack_fait"))
        return -1

    except KeyboardInterrupt:   #ctr-C abord
        save_at("DFS_stack_roue", stack_roue)
        save_at("DFS_stack_fait", stack_fait)
        save_at("DFS_ctr", ctr)
        return "configue sauvegardée !"


################Version avec k changement de cran pas forcément consécutif################
def tourner_roue(original, decalage, r, n):
    """roue contient un 0 si on ne la tourne pas, un 1 si on la tourne"""
    return tuple([(original[roue]+decalage[roue])%n for roue in range(r)])

def recherche_k_roue(r=3, n=5, k=2):
    """recherche un chemin passant par le plus de combinaison"""
    CTR = 0
    PROFONDEUR_MAX = 0
    DECALAGE = tuple([1]*k + [0]*(r-k))
    FREQUENCE = 100000
    sys.setrecursionlimit(n**r + 42) 


    def recurence(deja_fait, derniere_combinaison, profondeur=0):
        nonlocal CTR, PROFONDEUR_MAX, DECALAGE, FREQUENCE
        if CTR%FREQUENCE==0 or profondeur > PROFONDEUR_MAX:
            if profondeur > PROFONDEUR_MAX:
                PROFONDEUR_MAX = profondeur
                save_at("k_roue", repr((profondeur, derniere_combinaison, deja_fait)))

            if CTR%(FREQUENCE*15)==0:
                print('{:>12} | {:>12} | {:>12}'.format('ctr','deep','maxdeep'))
            print('{:>12} | {:>12} | {:>12}'.format(CTR,profondeur,PROFONDEUR_MAX))

        for deca in itertools.permutations(DECALAGE, r):
            CTR += 1
            nouvelle_combinaison = tourner_roue(derniere_combinaison, deca, r, n)
            if nouvelle_combinaison not in deja_fait :
                recurence(deja_fait.union((nouvelle_combinaison,)), nouvelle_combinaison, profondeur=profondeur+1)

    premier_cadena = tuple([0 for _ in range(r)])
    recurence({premier_cadena}, premier_cadena)
    print("fin !\nRegarde le fichier svg_data/k_roue pour le meilleur résultat ;)")

def recherche_combinaison_atteignable(r,n,k):
    fait = {tuple([0]*r)}
    decalage = tuple([1]*k + [0]*(r-k))

    def recu(combinaison):
        #on prend toutes les combinaisons de roue à tourner
        for deca in itertools.permutations(decalage, r):
            nouvelle_combinaison = tourner_roue(combinaison, deca, r, n)
            if not nouvelle_combinaison in fait:
                fait.add(nouvelle_combinaison)
                recu(nouvelle_combinaison)

    recu(tuple([0]*r))
    return fait

def tuples_short(tuples):
    return "".join(["("+",".join(map(str, t))+") " for t in tuples])

def rechercher(r,n,k):
    if r=="fin":return
    print(f"\nr {r}, n {n}, k {k}")
    sys.setrecursionlimit(n**r + 42) 
    accessible = recherche_combinaison_atteignable(r,n,k)
    non_accessible = []
    for combinaison in itertools.product(tuple(range(n)), repeat=r):
        if not combinaison in accessible: 
            non_accessible.append(combinaison)

    print("accessible :")
    print("\tlen  :",len(accessible))
    print("\tsomme:", sorted(set(map(sum, accessible))))

    non_accessible = tuple([ tuple(sorted(combi)) for combi in non_accessible])
    non_accessible = sorted(set(non_accessible))
    print("non accessible :")
    print("\tlen  :", len(non_accessible))
    print("\tsomme:", sorted(set(map(sum, non_accessible))))
    for groupe in range(0,len(non_accessible),20):
        print("\t",tuples_short(non_accessible[groupe:groupe+20]))

def appliquer_stratégie(strategie,r=3, n=3, cadena=None, debug=True, stop_zero=True):
    """changer cadena pour mettre une position de départ différent de (0,0,...)"""
    dernier_cadena = tuple([-1 for _ in range(r)])
    if cadena == None: 
        cadena = tuple([0 for _ in range(r)])
    fait = []

    CTR, FREQUENCE = 0, 100000

    while True :
        dernier_cadena = cadena
        cadena = strategie({"fait":fait, "cadena":cadena, "n":n, "r":r, "dernier_cadena":dernier_cadena})
        if cadena == tuple([0 for _ in range(r)]) :
            break
        if cadena in fait :
            break
        fait.append(cadena)

        if CTR%FREQUENCE==0:
            if CTR%(FREQUENCE*15)==0:
                print('{:>12} | {:>12}'.format('ctr','fait'))
            print('{:>12} | {:>12}'.format(CTR,len(fait)))
        CTR += 1

    return fait

def difference(cadena_ancien, cadena_nouveau):
    return tuple([cadena_nouveau[r]-cadena_ancien[r] for r in range(len(cadena_ancien))])
def verifie_cadena(cadena, n):
    return tuple([r%n for r in cadena])

def stratégie1(data):
    #on trie les cadenas par cran
    changement = difference(data["dernier_cadena"], data["cadena"])
    _, index = tuple(
        sorted([
            (cran, index)
            for index, cran in enumerate(data["cadena"])
            if changement[index] == 0
        ],reverse=True)
        )[0]

    cadena = list(data["cadena"])
    cadena[index] -= 1
    return verifie_cadena(cadena, data["n"])

resultat = appliquer_stratégie(stratégie1)
print("accèssible :",resultat)

#r le nb de roues
#n de crans
#k à tourner

# recherche_k_roue(r=3, n=5, k=2)


# for r,n,k in (
#     # (3, 4, 2), #toutes bombi dont la somme est impaire
#     # (3, 3, 2), #X
#     # (3, 6, 2),
#     # (4, 5, 3),
#     ("fin","","")
# ):
#     rechercher(r,n,k)


# while False and (saisie:=input("r,n,k :")) != "":
#     r,n,k = map(int, saisie.split())
#     rechercher(r,n,k)

# print(DFS(r=2,n=3, show_stack=True,frequence=1))#pour debug
# def main():
#     if input("résume ? (o/n) : ") == "n":
#         print("On commence à zéro")
#         print(DFS(r=3,n=3, resume=False))
#     else :
#         print("on re-commence")
#         print(DFS(r=3,n=3, resume=True))
# main()

# if __name__ == '__main__':
#     import doctest
#     doctest.testmod()
