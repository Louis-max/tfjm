def main() :
	n,d = int(input()), float(input ())# On entre n le nombre de pizza et d la d u r e de
	D = sorted ([ float(input()) for _ in range(n)])[:: -1]
	# on entre la duree de preparation de chaque pizza et on classe par ordre decroissant
	T_g , T_d = 0, 0 #initialisation des deux tas (respectivement tas gauche et tas droite)
	I_0 , I_1 = [], [] # c r a t i o n des deux ensembles contenant les pizzas
	while D: #Tant que D est non vide
		plus_petit_element = D[-1]
		if T_d + plus_petit_element < T_g:
			T_d += plus_petit_element
			I_1.append(plus_petit_element)
			D.pop()
			#Si la pizza est placee au plus proche de la date t = 0 lorsque elle est placee a gauche alors on ajoute la pizzas dans I_1
		else:
			plus_grand_element = D[0]
			if T_g + plus_grand_element <= d:
				T_g += plus_grand_element
				I_0.append(plus_grand_element)
				D.pop (0)
				#Sinon , si cela est possible on ajoute la pizzas restante ayant la duree de p r p a r a t i o n la plus longue dans I_0
			else:
				break

	I_1 += D
	I_1 = sorted(I_1)[:: -1]
	#On ajoute les pizzas restantes de D dans I_1 et on trie a nouveau par ordre d c r o i s s a n t

	print("L’ensemble I_0 contient les pizzas", I_0)
	print("L’ensemble I_1 contient les pizzas", I_1)
	print("Le nombre pizzas placees durant la phase de p r p a r a t i o n est : m = ", len(I_0))

main()