import copy
import itertools
SYMBOLE = {3: " ", 2: "·", 1: "¤", "impossible": "█"}

#Nos programmes python pour une 3 coloration

#####################foncitons basiques#####################

def get_couleur(p):
    """
    >>> [get_couleur(i) for i in range(4)]
    [0, 1, 2, 0]
    """
    return p % 3


def get_hauteur(p):
    """
    >>> [get_hauteur(i) for i in range(4)]
    [0, 0, 0, 1]
    """
    return p // 3


def get_participant(c, h):
    """
    >>> [get_participant(*decompose(i)) for i in range(4)]
    [0, 1, 2, 3]
    """
    return h * 3 + c

def flatten_list(table):
    """flatten
    >>> flatten_list([[1,2,3],[4]])
    [1, 2, 3, 4]
    """
    return [p for couleur in table for p in couleur]

def decompose(participant):
    """
    >>> [decompose(i) for i in range(4)]
    [(0, 0), (1, 0), (2, 0), (0, 1)]
    """
    return get_couleur(participant), get_hauteur(participant)

def Leurcou(Pc, Oc):
    """renvoie la couleur qui n'est n'y celle de l'objectif n'y celle du participant"""
    s = tuple(sorted([Pc, Oc]))
    return {
        (0, 1): 2,
        (1, 2): 0,
        (0, 2): 1
    }[s]


#####################affichage#####################
def tab_all(text):
    return text.replace("\n", "\n\t")

def merge_fake_print(*groupes, sep="   |   "):
    """permet d'assembler des groupes d'affichage"""
    largeur_max = [
        max([
            len(row)
            for row in g
        ]) for g in groupes
    ]

    groupes = [[line.rstrip() for line in g] for g in groupes]

    nb_max = max([len(g) for g in groupes])
    for g in range(len(groupes)):
        groupes[g].extend([""] * (nb_max - len(groupes[g])))

    mise_en_forme = sep.join(["{:<" + str(largeur) + "}" for largeur in largeur_max])
    merge = [
        mise_en_forme.format(*[groupe[rang] for groupe in groupes]) + "\n"
        for rang in range(nb_max)
    ]

    return merge

def print_text(groupe):
    print("".join(groupe).rstrip())

def Mstring(matrice, largeur=1, x_label="x", y_label="y", translate=lambda x: x):
    """renvoie le text d'une matrice bien mise en forme"""
    result = [""]

    def fake_print(*args, end="\n", sep=" "):
        result[-1] += sep.join(map(str, args)) + end
        if end == "\n": result.append("")

    original = lambda x: x
    if translate != original:
        n = len(matrice[0])
        matrice = [
            [matrice[translate(debut)][translate(fin)]
             for fin in range(n)
             ] for debut in range(n)
        ]

    afficher_elem = "{:>" + str(largeur) + "}"
    afficher_abscisse = '{:>' + str(largeur) + '}'

    fake_print("", y_label)
    for y, ligne in enumerate(matrice[::-1]):
        y = len(matrice) - y - 1
        fake_print('{:>3} | '.format(translate(y)), end="")
        for element in ligne:
            fake_print(afficher_elem.format(element), end="")
        fake_print()

    fake_print("----|", "-" * largeur * len(matrice[0]), sep="-")
    fake_print("    | ", end="")

    for x in range(len(matrice[0])):
        fake_print(afficher_abscisse.format(translate(x)), end="")

    fake_print(" ", x_label)
    return result


def afficher_matrice(matrice, largeur=4, x_label="x", y_label="y", translate=lambda x: x):
    print("".join(Mstring(matrice, largeur=largeur, x_label=x_label, y_label=y_label, translate=translate)))


def Synthese(liaisons):
    """renvoie la synthese des liaisons
    synthese[(debut, fin)]=min(p)
    """
    synthese, hauteur = {}, len(liaisons[0])
    for c in range(3):
        for h in range(hauteur):
            for Cible_p, Cible_c, Cible_h in liaisons[c][h]:
                token = (h * 3 + c, Cible_h * 3 + Cible_c)
                if token not in synthese.keys():
                    synthese[token] = Cible_p
                synthese[token] = min(synthese[token], Cible_p)
    return tuple(sorted(synthese.items()))


def afficher_compres(synthese):
    fleche = {1: "-->", 2: "~> "}
    return (
            "[" +
            ", ".join(sorted([str(debut) + " => " + str(fin) for (debut, fin), p in synthese if p == 1])) +
            ", ".join(sorted([str(debut) + " -> " + str(fin) for (debut, fin), p in synthese if p == 2])) +
            "]")

def compress_objectifs(objectifs):
    """covnertie l'index des objectifs en couple hauteur couleurs"""
    return tuple([
        tuple([
            (get_couleur(objectif), get_hauteur(objectif))
            for objectif in couleur
        ]) for couleur in objectifs
    ])

def rechercher_table(objectifs_compresse, show=False, debug=False):
    """
    P pour Premier
    I pour intermediaire
    O pour objectif (de P)
    Le chemin fait :
       P
    -> P (en dessous)
    -> I
    -> I (en dessous)
    -> O (au dessus)
    -> O
    """

    # compression
    NB_COULEURS = 3
    HAUTEUR_MAX = len(objectifs_compresse[0])
    OBJECTIFS = objectifs_compresse
    resultat = []

    # fonctions
    def interference(liaisons, Pc, Ph, Ic, Ih, Oc, Oh):
        """renvoie true si mettre le joueur I en intermediaire créer des interferences"""

        # liaison 1
        Pdessous = (Ph + 1) % HAUTEUR_MAX
        for p, c, h in liaisons[Pc][Pdessous]:
            if p == 1: return True
            if (p == 2 and c == Ic) and (h != Ih):
                return True  # les deux liaisons n'ont pas le même objectif

        # Liaison 2
        Idessous = (Ih + 1) % HAUTEUR_MAX
        Odessus = (Oh - 1) % HAUTEUR_MAX
        for p, c, h in liaisons[Ic][Idessous]:
            if (c == Oc and h != Odessus):
                return True  # les deux liaisons n'ont pas le même objectif

        return False

    def recursion(liaisons_original, participant):
        Pc, Ph = decompose(participant)
        if participant == HAUTEUR_MAX * NB_COULEURS - 1:
            resultat.append(Synthese(liaisons_original))
            if show: print("\t", afficher_compres(Synthese(liaisons_original)))
            return

        Oc, Oh = OBJECTIFS[Pc][Ph]
        Ic = Leurcou(Pc, Oc)
        for Ih in range(HAUTEUR_MAX):
            if not interference(liaisons_original, Pc, Ph, Ic, Ih, Oc, Oh):  # on peut le rajouter
                liaisons = copy.deepcopy(liaisons_original)
                Pdessous = (Ph + 1) % HAUTEUR_MAX
                Idessous = (Ih + 1) % HAUTEUR_MAX
                Odessus = (Oh - 1) % HAUTEUR_MAX

                liaisons[Pc][Pdessous].append((1, Ic, Ih))
                liaisons[Ic][Idessous].append((2, Oc, Odessus))
                recursion(liaisons, participant + 1)

    # initialisation
    liaisons_vierge = \
        tuple([
            tuple([
                [] for _ in range(HAUTEUR_MAX)
            ]) for _ in range(NB_COULEURS)
        ])
    if debug: print("objectifs :", objectifs)
    if debug: print("listes possibles :")
    recursion(liaisons_vierge, 0)
    if debug: print("fin")
    return resultat

def matrice_cumulation(synthese, n, par_couleur=True):
    matrice = [
        [3
         for _ in range(n)
         ] for _ in range(n)
    ]
    for tableau in synthese:
        for (debut, fin), p in tableau:
            matrice[debut][fin] = min(matrice[debut][fin], p)

    for debut in range(n):
        for fin in range(n):
            if get_couleur(debut) == get_couleur(fin):
                matrice[debut][fin] = SYMBOLE["impossible"]
            else:
                matrice[debut][fin] = SYMBOLE[matrice[debut][fin]]

    return matrice

def matrice_objectif(objectifs, n):
    n = len(objectifs[0]) * 3
    matrice = [
        [
            ""
            for fin in range(n)
        ] for debut in range(n)
    ]
    for c, couleur in enumerate(objectifs):
        for h, objectif in enumerate(couleur):
            matrice[get_participant(c, h)][objectif] = SYMBOLE[1]

    for debut in range(n):
        for fin in range(n):
            if get_couleur(debut) == get_couleur(fin):
                matrice[debut][fin] = SYMBOLE["impossible"]
    return matrice

def ParCouleur(x):
    return 3 * (x % 3) + x // 3

def main():
    for objectifs, label in [
        ([[1, 5, 4], [2, 8, 6], [0, 3, 7]], "2, 3 et 4 cycle."),
        ([[1, 5, 8], [2, 0, 3], [4, 6, 7]], "4 et 5 cycles"),
        ([[1, 4, 7], [2, 5, 8], [0, 3, 6]], "3,3,3 cycles"),
        # ([[1,4,7,10],[2,5,8,11],[3,6,9,0]], "12 cycles, une seule solution"),
        # ([[1,4,8],[2,5,0],[3,6,7]], "9 cycles, arrangé à la fin"),
        # ([[1,4,8],[2,5,0],[3,6,7]], "9 cycles, en inversant le 7 et le 8"),
        # ([[8,2,5],[0,3,6],[1,4,7]], "9 cycles dans l'autre sens -> résultat normale"),
        # ([[1,4,7,10],[2,5,8,11],[0,3,6,9]], "3,3,3,3 cycles"),
        # ([[1,4,7,11],[2,5,8,9],[0,3,6,10]], "3,3,3,3 cycles (dont un dans l'autre sens)"),
        ("fin", "fin")
    ]:
        if objectifs == "fin": continue
        h = len(objectifs[0])
        n = h * 3
        Cobjectifs = compress_objectifs(objectifs)
        Mobjectifs = matrice_objectif(objectifs, n)

        synthese = rechercher_table(Cobjectifs)
        print("objectifs:", objectifs, "\t(", len(synthese), ") :", label)

        synthese = [synthese[0]] if synthese else []
        Msynthese = matrice_cumulation(synthese, n)
        if len(synthese) <= 2:
            for s in synthese: print("->", s)

        Groupe1 = merge_fake_print(["matrice d'objectif :\n"] + Mstring(Mobjectifs, translate=ParCouleur),
                                   ["matrice liens nécéssaires:\n"] + Mstring(Msynthese, translate=ParCouleur))
        Groupe2 = merge_fake_print(["matrice d'objectif :\n"] + Mstring(Mobjectifs),
                                   ["matrice liens nécéssaires:\n"] + Mstring(Msynthese))
        print_text(merge_fake_print(Groupe1, Groupe2, sep="   ||   "))

    if False:
        if synthese:
            for k, s in enumerate(synthese):
                print("{:>4} : ".format(k), afficher_compres(s))
            tikz_descendance(synthese)
        else:
            print("rien trouvé :'(")


main()

if __name__ == '__main__':
    import doctest
    doctest.testmod()
