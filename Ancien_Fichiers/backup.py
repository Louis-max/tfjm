ef tikz_descendance(synthese):
    dirpath, filename = os.path.split(os.path.abspath(__file__))
    synthese = [
        [
            t
            for t,p in table if p == 1
        ]   for table in synthese
    ]

    def recurence(restant,etat="racine"):
        if restant == [[]] :
            return "\nnode[noeud_fin]{} edge from parent[arc_oui] node[near start, left] {oui}"

        maximum = most_frequent(flatten_list(restant))

        #on sépare en deux groupes
        separation = {True:[], False:[]}
        for pref in restant :
            separation[maximum in pref].append([p for p in pref if p != maximum])

        #on tikze
        result = "\nnode{" + str(maximum) + "}"
        if etat == "racine" :
            pass
        elif etat == True :
            result += "\nedge from parent[arc_oui] node[near start, left] {oui}"
        elif etat == False :
            result += "\nedge from parent[arc_non] node[near start, right] {non}"

        for enfant in (True, False):
            if separation[enfant] :
                result += "\nchild\n{"
                result += tab_all(recurence(separation[enfant], etat=enfant))
                result += "\n}"

        return result


    resultat = "\\begin{tikzpicture}[binary tree layout]"
    resultat += tab_all("\n\\"+recurence(synthese).lstrip())
    resultat += ";\n\\end{tikzpicture}"
    with open(os.path.join(dirpath, "couleurs", "descendance.tex"), 'w+') as f:
        f.write(resultat)


\\Notons $g$ le nombre de groupe de chocolats ratées consécutifs. Et $Card(G_x)$ la taille du groupe $G_x$. Le nombre de pesées nécéssaire est de :
\\$\sum_{n=0}^{g-1} 2\ceil{\log_2(Card(G_n))} + 1$
\\La plus grande puissance $p$ de $2$ atteignable pour chaque groupe est de :
\\$p=\ceil{\log_2(\frac{k}{g})}$, il restera alors $r$ chocolats $r=k - g* 2^p$.
\\Nous pouvons maintenant calculer le nombre de groupe $g'$ qui pourront bénéficier d'une puissance supplémentaire :
\\$g' = \ceil{\frac{r}{2^p}}$
\\Le nombre de pesées totale est alors de :
\\$\Bigg(\sum_{n=0}^{g-g'-1} 2\ceil{\log_2(Card(G_n))} + 1 \Bigg)+\Bigg(\sum_{n=0}^{g'-1} 2\ceil{\log_2(Card(G_n)\times 2)} + 1\Bigg)$
\\$(g-g'-1)+ (g'-1) + \Bigg(\sum_{n=0}^{g-g'-1} 2\ceil{\log_2(2^p)}\Bigg)+\Bigg(\sum_{n=0}^{g'-1} 2\ceil{\log_2(2^{p+1})}\Bigg)$
\\$g - 2 + 2\Bigg(\sum_{n=0}^{g-g'-1} \ceil{p}\Bigg)+2\Bigg(\sum_{n=0}^{g'-1} \ceil{p+1}\Bigg)$
\\$g - 2 + 2\Bigg(\sum_{n=0}^{g-g'-1} \ceil{p} + \sum_{n=0}^{g'-1} \ceil{p+1}\Bigg)$
\\$g - 2 + 2\bigg((g-g'-1)\ceil{p} + (g'-1)\ceil{p+1}\bigg)$
\\$g - 2 + 2\bigg((g-g'-1)\ceil{p} + (g'-1)\ceil{p+1}\bigg)$
\\$g - 2 + 2\bigg((g-g'-1)\ceil{p} + (g'-1)\ceil{p} + g' - 1 \bigg)$
\\$g - 2 + 2\bigg((g-g'-1 + g' - 1)\ceil{p} + g' - 1 \bigg)$
\\$g - 2 + 2\bigg((g-2)\ceil{p} + g' - 1 \bigg)$


(g-2)log2(k/g) + r/(2^p)


k(g -4)/(g2^(k/g))

#backup
def to_tikz(synthese, hauteur, auto_import=False):
    """
    \\begin{tikzpicture}[x=0.5cm,y=0.5cm, scale=4]
        \\node[Participantn] (P0) at (0,2) {$P_x$};
        \\node[Participantn] (P1) at (1,2) {$P_y$};
        \\node[Participantn] (P2) at (2,2) {$P_z$};
        
        \\draw[FlecheA, ->] (P0) -- (P1);
        \\draw[FlecheA, ->] (P1) -- (P2);
        \\draw[FlecheC=colorV, ->] (P2) to[out=180-45,in=45,  looseness=1] (P0);
        \\draw[FlecheC=colorR, ->] (P2) to[out=180+45,in=-45, looseness=1] (P1);
    \\end{tikzpicture}
    """
    #init
    if auto_import :
        importation = ", Participantn/.style={circle, minimum size=1cm, draw=black},FlecheA/.style={->, >=latex, green, line width=1pt},FlecheB/.style={->, >=latex, blue, line width=0.8pt},FlecheC/.style={->, >=latex, yellow, line width=0.6pt}"
    else :
        importation = " "
    result = "\\begin{tikzpicture}[x=0.5cm,y=0.5cm, scale=4 "+ importation +"]\n"

    #noeuds
    for p in range(hauteur*3):
        result += to_string("\t\\node[Participantn] (P",p,") at (",get_couleur(p),",",-get_hauteur(p),") {", p,"};")

    #liaison 0
    result += "\n"
    for h in range(hauteur-1):
        for c in range(3):
            result += to_string("\t\\draw[FlecheA, ->] (P",(h+1)*3+c,") to (P",h*3+c,");")

    #liaisons
    result+="\n"
    for (debut, fin), p in synthese:
        fleche = {1:"FlecheB", 2:"FlecheC,draw opacity=0.2"}
        result += to_string("\t\\draw[",fleche[p],", ->] (P",debut,") to[bend left=10] (P",fin,");")

    #fin
    result += "\\end{tikzpicture}\n"
    return result

def generate_tikz_file(synthese, hauteur):
    dirpath, filename = os.path.split(os.path.abspath(__file__))

    for k, s in enumerate(synthese) :
        with open(os.path.join(dirpath, "couleurs", "graphe"+str(k)+".tex"), 'w+') as f:
            f.write(to_tikz(s, hauteur))

    résumé = ""
    for groupe in range(0, len(synthese)//3):
        résumé += "\\begin{figure}[ht]\n\t\\centering"
        for k in range(3):
            if groupe*3 + k >= len(synthese): pass
            résumé += "\\hfill\n\t\\begin{subfigure}{0.25\\textwidth}\n\t\t\\resizebox{0.9\\textwidth}{!}{\\input{images/"
            résumé += "graphe" + str(groupe*3 + k)
            résumé += "}}\n\t\t\\caption{" + "graphe" + str(groupe*3+k) + "}\n\t\\end{subfigure}"
        résumé += "\n\\end{figure}\n\n"
    
    with open(os.path.join(dirpath, "couleurs", "récap.tex"), 'w+') as f:
        f.write(résumé)

    print("fin génération !")

#met à jour les fichiers tikz brutes
# generate_tikz_file(synthese, len(objectifs[0]))
# generate_tikz_file(synthese, len(objectifs[0]), auto_import=True)