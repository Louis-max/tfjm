import os
import time
import math
import argparse
import traceback
from utils import *
from class_multidimensionalarray import *
from class_latex import *
from main import *

def walk(top):
    """applique le convertisseur sur tout les fichiers enfants"""
    def recursive(path):
        for name in os.listdir(path):
            children = os.path.join(path, name)
            if os.path.isdir(children):
                for flatten in recursive(children): 
                    yield flatten
            else:
                yield str(children)
    return recursive(top)

def main(path):
    """exec lors du command line"""
    error_log = ""
    ctr = 0
    for path_file in walk(path):
        if os.path.basename(path_file).startswith("image "):
            try :
                ###open###
                with open(path_file, "r") as file:
                    content = list(file.readlines())
                    category = content[0].strip()
                    params = eval("".join(content[1::]))
                output=""
                ctr += 1
                print("Get:",ctr," ",path_file, end=" ")

                ###take generator###
                if  category == "demo":
                    output = generator_demo(params)

                elif category == "cycle":
                    output = generator_cycle(params)

                elif category == "pavage carré" :
                    output = generateur_pavage_carre(params)
                
                elif category == "pavage carré moitié" :
                    output = generateur_pavage_carre_moitie(params)

                elif category == "grenouille ligne":
                    output = geneteur_grenouille_ligne(params)

                elif category == "seigneur chilli":
                    output = generateur_chilli(params)

                else:
                    error_log += "["+str(path_file)+"]" +"\n no converter for this category ("+str(category)+")\n"
                    continue

                ###save###
                name = os.path.basename(path_file)      #get name
                name = name.split("image ")[1]          #remove image
                name = name.split(".")[0] + ".tikz"     #add .tikz
                #output, log = tikz_beautifier(output, "no_color", "no_clip", no_strip=True) #beautifier <3
                #error_log += log

                with open(os.path.join(os.path.dirname(path_file), name), 'w+') as file:
                    file.write(output)

                print("Done. ✅")

            except:
                print("Error. 🛑")
                error_log += "["+str(path_file)+"]" +"\n" + traceback.format_exc() + "\n"

    if error_log != "":
        print("\n"*2)
        print("Error log : \n",error_log)
    print("end")

HIDE = ("H", "h", " ") #utiliser H, h ou " " pour ne rien afficher
template = {
    "noeud":{
        "o":"nodeN",
        "A":"nodeA",
        "B":"nodeB",
        "a":"nodeC=colorA",
        "b":"nodeC=colorB",
        "c":"nodeC=colorC",
        "d":"nodeC=colorD",
        "#":"nodeC=colorG",
        "g":"nodeC=colorG"
    },

    "seigneur":{
        "M":"nodeC=colorR",
        "O":"nodeN",
        "A":"nodeC=colorA",
        "B":"nodeC=colorB",
        "C":"nodeC=colorC",
        "D":"nodeC=colorD",
        "G":"nodeC=colorG",
        "R":"nodeC=colorR"
    },

    "seigneur_ligne":{
        "M" : "colorR, line width=1.5pt",
        "-" : "",
        "A" : "colorA",
        "B" : "colorB",
        "C" : "colorC",
        "D" : "colorD",
        "R" : "colorR"
    },

    "line-":{
        "-" : "",
        "#" : "colorG",
        "a" : "colorA",
        "b" : "colorB",
        "c" : "colorC",
        "d" : "colorD",
        "R" : "colorR, line width=1.5pt",
        "=" : "colorR, line width=1.5pt",
        "D" : "dashed",
        "d" : "dashed",
        "." : "dashed"
    },

    "line|":{
        "|" : "",
        "#" : "colorG",
        "R" : "colorR, line width=1.5pt",
        "§" : "colorR, line width=1.5pt",
        "D" : "dashed",
        "d" : "dashed",
        "." : "dashed"
    },

    "fill":{
        "A" : "fill=colorAlight",
        "a" : "fill=colorAlight",
        "B" : "fill=colorBlight",
        "b" : "fill=colorBlight",
        "C" : "fill=colorClight",
        "c" : "fill=colorClight",
        "D" : "fill=colorDlight",
        "d" : "fill=colorDlight",
        "#" : "fill=colorg"
    }
}

def generator_demo(params):
    return "Petite démo :\n"+str(repr(params))

def to_string(*args):
    args = list(args)
    for i in range(len(args)):
        if isinstance(args[i], float):
            if int(args[i]) == args[i]:
                args[i] = int(args[i])
    return "".join(map(str, args))

def geneteur_grenouille_ligne(params):
    """grenouille k=x"""
    params = [
        " ." + ligne + ". "
        for ligne in params
    ]

    largeur = len(params[0])
    hauteur = len(params   )
    params.insert(0, " "*largeur)
    params.insert(0, " "*largeur)
    # params.append(   " "*largeur)
    # params.append(   " "*largeur)
    return generateur_pavage_carre(params,clip_min=(0.5,-0.5),clip_max=(largeur//2-0.5, hauteur//2))

def generateur_chilli(params):
    tikz = to_string("\\begin{tikzpicture}[baseline]\n")

    x = 0

    for p in range(len(params)):
        taille_debut, cate, taille_fin = params[p]
        cate = cate.upper()
        if cate not in template["seigneur"].keys() :
            raise Exception("erreur seigneur",cate,"n'a pas de template"); 
            continue
        if cate not in template["seigneur_ligne"].keys() : 
            raise Exception("erreur seigneur ligne",cate,"n'a pas de template"); 
            continue

        type_ligne = template["seigneur_ligne"][cate]
        type_seigneur = template["seigneur"][cate]
        if p == 0: #sep debut
            tikz += to_string("\\draw[",type_ligne ,"](", x, ",0) -- +(0,0.4) -- +(0,-0.4);\n\n")


        ajout = to_string(" node[midway,below] {",taille_debut,"}") if taille_debut != 0 else ""
        tikz += to_string("\\draw[", type_ligne, "](", x, ",0) -- ++(",taille_debut,",0)",ajout ,";\n")
        x += taille_debut

        tikz += to_string("\\draw (", x, ",0) node [",type_seigneur, "] {};\n")
        tikz += to_string("\\draw (", x, ",0.4) node {$S_{",p ,"}$};\n")

        ajout = to_string(" node[midway,below] {",taille_fin,"}") if taille_fin != 0 else ""
        tikz += to_string("\\draw[", type_ligne, "](", x, ",0) -- ++(",taille_fin,",0) ", ajout,";\n")
        x += taille_fin

        #petit séparateur
        tikz += to_string("\\draw(", x, ",0) -- +(0,0.2) -- +(0,-0.2);\n")
        tikz += "\n"
    
        #separateur fin
        if p == len(params): 
            tikz += to_string("\\draw[",type_ligne ,"](", x, ",0) -- +(0,0.4) -- +(0,-0.4);\n\n")



    hauteur = 0.3
    tikz += to_string("\\clip (-",hauteur,",-",hauteur,") rectangle (",x+hauteur ,",",hauteur,");\n")

    tikz += "\n\\end{tikzpicture}"


    return tikz


def generateur_pavage_carre(params,clip_min=(0,0),clip_max=None):
    """generatuer pour pavage carré, de la forme :
    n-n-n-n
    |f|f|f|
    n-n-n-n
    """
    largeur = (len(params[0])-1)/2
    hauteur = (len(params)   -1)/2
    if clip_max == None :
        clip_max = (largeur, hauteur)
        
    tikz = to_string("\\begin{tikzpicture}[baseline]\n\\clip ", repr(clip_min), " rectangle ",repr(clip_max) ,";\n")
    lines = []

    for y, ligne in enumerate(params):
        y = len(params) - 1 - y
        for x, token in enumerate(ligne):
            if y%2 == 0:    #horizontale
                if x%2 == 0:#noeud
                    if token in HIDE : continue
                    type_noeud = template["noeud"][token]
                    lines.append([2,to_string("\\node[",type_noeud, "] at (", x/2,",",y/2,") {};")])

                else:       #arrête -
                    if token in HIDE : continue
                    if token == "+"  : #style avec croix en rouge  
                        lines.append([1,to_string("\\draw[colorR,line width=1.5pt](", (x-1)/2+0.5, ",", y/2, ") edge +(0.18,0.18) edge +(-0.18,-0.18);")])
                        token = "-"

                    type_line = template["line-"][token]
                    lines.append([1,to_string("\\draw[",type_line,"](", (x-1)/2, ",", y/2, ") edge +(1,0);")])

            else:           #vertical
                if x%2 == 0:#arrête |
                    if token in HIDE : continue
                    if token == "+" :
                        lines.append([1,to_string("\\draw[colorR,line width=1.5pt](", x/2, ",", (y-1)/2+0.5, ") edge +(0.18,0.18) edge +(-0.18,-0.18);")])
                        token = "|" #style avec croix en rouge

                    type_line = template["line|"][token]
                    lines.append([1,to_string("\\draw[",type_line,"](", x/2, ",", (y-1)/2, ") edge +(0,1);")])

                else:       #fill
                    if token == "." : continue
                    if token in HIDE: continue
                    if token in ("=","-","+"):  #tracer ligne horizontale rouge
                        lines.append([1,to_string("\\draw[colorR, line width=1.5pt](", (x-1)/2, ",", (y-1)/2+0.5, ") edge +(1,0);")])
                        if token == "+":        #croix = ligne horizontale + verticale
                            token = "§"                            
                    if token in ("§","|"):      #ligne verticale
                        lines.append([1,to_string("\\draw[colorR, line width=1.5pt](", (x-1)/2+0.5, ",", (y-1)/2, ") edge +(0,1);")])
                    elif token in template["fill"].keys() :
                        type_fill = template["fill"][token]
                        lines.append([0,to_string("\\path[", type_fill, "](", (x-1)/2, ",", (y-1)/2, ") -- ++(0,1) -- ++(1,0) -- ++(0,-1) -- ++(-1,0) -- cycle;")])

    if "#" in params[0] :   #je sait plus pourquoi ...
        lines.append([0.5, to_string("\\path[fill=colorg](0,",hauteur,") -- ++(",largeur,",0) -- ++(0,",-hauteur,") -- (0,",hauteur,") -- cycle;")])
        lines.append([1.5, to_string("\\draw[colorR,line width=1.5pt](0,",hauteur,") -- (",largeur,",0);")])

    ###tris et assemblage###
    tikz += "\n".join([line for priority, line in sorted(lines,key=lambda p_l: p_l[0])])
    tikz += "\n\\end{tikzpicture}"
    return tikz

def generateur_pavage_carre_moitie(params):
    params = [
        [c for c in line]
        for line in params
    ]

    for y in range(len(params)):
        for x in range(y+1, len(params[0])):
            params[y][x]="#"

    params = [
        "".join(line)
        for line in params
    ]

    return generateur_pavage_carre(params)

def generator_cycle(params):
    n = len(params)
    rayon = 1.7

    tikz="\\begin{tikzpicture}[x=0.5cm,y=0.5cm]\n"
    angle_ancien = 0
    for i in range(n):
        angle = int(360/n * i) + 90
        nom = "{" + params[i] + "}"
        tikz += f"\\node (N{i}) at ({angle}:{rayon}) {nom};\n"

    for i in range(n):
        tikz += f"\\draw [->] (N{i}) -- (N{(i+1)%n});\n"

    tikz += f"\\clip(-{rayon+2},-{rayon+2}) rectangle ({rayon + 2},{rayon + 2});\n"
    tikz += "\\end{tikzpicture}"
    return tikz

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Generate Tikz code",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('path', type=str, help="the path of the file to convert")

    options = {}
    for keys, value in vars(parser.parse_args()).items():
        options[keys] = value
    main(options["path"])