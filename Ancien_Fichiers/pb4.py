import itertools
from math import ceil, floor, log
from utils import *
import os
import time
import csv
import re
import traceback
# traceback.format_exc()

def append_at(name, key, data):
    registre = load_at(name)
    registre[key] = data
    save_at(name, registre)

def balance(data, a1, a2, b1, b2):
    somme_a = sum(data[a1:a2:])
    somme_b = sum(data[b1:b2:])
    if somme_a == somme_b:
        return "="
    if somme_a < somme_b :
        return "<"
    return ">"

def recherche_dico(data, debut, fin, majoration, deep=0, debug=False, majoration_exacte=False):
    """renvoit le nombre de choco défectueux, le nombre de pesé dans la branche enfant ainsi que leurs positions"""
    if deep==0:
        nb_toto = ceil(log(fin, 2))
        ajout = len(data) - 2**nb_toto
        data.extend([0 for _ in range(ajout)])
        fin = len(data)

    pesé = 0
    if majoration == 0:
        return 0, pesé, []

    if debug:print("\t"*deep, "debut/fin/majoration",debut, fin, majoration)

    if fin == debut+1 or (majoration_exacte and fin == debut+majoration) :
        if debug:print("\t"*deep, "###trouvé! à pos",debut)
        return majoration, pesé, list(range(debut, fin))

    Rdebut, Rmillieux, Rfin = debut, ceil((debut+fin)/2), fin

    premier = (Rdebut, Rmillieux)
    deuxieme = (Rmillieux, Rfin)

    pesé += 1
    valeur1 = majoration
    coté = balance(data, Rdebut, Rmillieux, Rmillieux, Rfin) 
    if debug:print("\t"*deep ,coté)
    if coté == ">" or coté == "=" :
        if coté == "=" :
            valeur1 = floor(majoration/2)
        plus_lourd = premier
        plus_legée = deuxieme
    else :
        plus_lourd = deuxieme
        plus_legée = premier


    resultat1 = recherche_dico(data, *plus_lourd , valeur1 , deep=deep+1, debug=debug)
    resultat2 = recherche_dico(data, *plus_legée, majoration - resultat1[0], deep=deep+1, debug=debug, majoration_exacte=True)

    return (resultat1[0] + resultat2[0], resultat1[1] + resultat2[1] + pesé, resultat1[2] + resultat2[2])

def benchmark(fonction, n, k):
    """renvoie le nombre de pesé pour toutes les bandes de test de n choco et k ratés"""
    result = []

    def generator(n, k, poids_raté=-1):
        """renvoie les datas qui contiennent exactement k choco defectueux"""
        if poids_raté == -1:
            poids_raté = round(1 + 1/(n+3),4)

        choix = tuple(range(n))
        for stack in itertools.combinations(choix, k):
            chocos = [1 for _ in range(n)]
            for r in stack:
                chocos[r] = poids_raté
            yield chocos

    for chocos in generator(n,k):
        nb_defectueux, nb_pesée, emplacement = fonction(chocos, 0, n, k)

        if nb_defectueux != k:
            print("Erreur :",nb_defectueux, "trouvé contre", k,"normalement", chocos, emplacement)
            _ = fonction(chocos, 0, n, k, debug=True)
            return

        for pos in emplacement:
            if chocos[pos] == 1:
                print("Erreur : choco n°",pos, "n'est pas raté", chocos, emplacement)
                _ = fonction(chocos, 0, n, k, debug=True)
                return

        result.append(nb_pesée)

    return result


def chercheur(n_max=-1):
    t = time.time()
    n = 1
    while True :
        if n == n_max :
            break
        n, k = n+1, 0
        print("[{:>5}] debut n={}".format(round(time.time() - t,2), n))
        line      = {}
        line_comp = {}
        while k < n :
            print("\t\t[{:>5}] debut n={} \t k={}".format(round(time.time() - t,2), n, k))
            k += 1
            resultat = benchmark(recherche_dico, n, k)
            moyenne = round(sum(resultat)/len(resultat),2)
            minn = min(resultat)
            maxx = max(resultat)

            line[k] = resultat
            line_comp[k] = (minn, moyenne, maxx)
        
        append_at("pb4_toto",str(n), line)
        append_at("pb4_comp",str(n), line_comp)

def convertisseur(name, extract=2):
    data = load_at(name)
    n = max(map(int, data.keys())) + 1

    matrice = [[" " for _ in range(n)] for _ in range(n)]
    for n, ligne in data.items():
        for k, data in ligne.items():
            matrice[int(n)][int(k)] = int(data[extract])

    with open(f"{name}_extracted.csv","w+") as my_csv:
        newarray = csv.writer(my_csv,delimiter=',')
        newarray.writerows(matrice)

# chercheur(n_max=10)
# convertisseur("pb4_comp")

def calculer(n,k):
    """calcule pour 2^n et 2^k choco"""
    if k>n:
        return ""
    k = min(k, n-k)
    somme = 0
    for i in range(1, k):
        somme += (n-i)* 2**(i-1)
    return somme

n = 20
matrice = \
    [
        [calculer(x,y)  for x in range(n)] 
        for y in range(n)
    ]

#afficher_matrice(matrice, largeur=8, x_label="2^n", y_label="2^k")
# print(benchmark(recherche_dico, 8, 3))

from decimal import Decimal

def temps(h,k):
    toto = h*k - k + 1
    for t in range(1, k-1):
        toto -= floor(log(t,2))

    return toto * 1

liste_expo = (2,4,8,16)
color = {2:"colorA",4:"colorB",8:"colorC",16:"colorD"}
data = {i : [] for i in liste_expo}

from math import ceil, log
import random
print("""
\\begin{tikzpicture}
\\begin{loglogaxis}[
    xmin=1, ymin=1,
    xlabel={n},
    ylabel={Nombre de pesées},
    legend pos=south east,
]""")
for log_k in (2,4,8,16) :
    print("\\addplot[mark=none, smooth] coordinates{",end="")
    points = []
    for echantillon in range(300) :
        log_2 = random.uniform(1, 50)
        h = ceil(log_2)
        n = int(2**log_2)
        k = int(log(n, log_k))
        if k<=0: continue;
        nb_pesée = 2*(2**(floor(log(k,2))+1)-1 + (h - floor(log(k,2))-1)*k)
        points.append((n,nb_pesée))
    print(" ".join([
            f"({n}, {nb_pesée})" for n, nb_pesée in sorted(points)
        ]),"};")
    points = []
print("""
\\legend{$k=log_2(n)$,$k=log_4(n)$,$k=log_8(n)$,$k=log_{16}(n)$}
\\end{loglogaxis}
\\end{tikzpicture}
""")













