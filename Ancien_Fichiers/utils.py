from collections import deque       #BFS iterator
import argparse                     #command line argument
import csv                          #read color data
import os.path                      #open files to convert
import os                           #find cwd
import re                           #find regex
import sys                          #command line argument
import time                         #record performance

def merge_fake_print(*groupes, sep="   |   "):
    largeur_max = [
        max([
            len(row)
            for row in g
        ])  for g in groupes
    ]

    groupes = [[line.rstrip() for line in g] for g in groupes]

    nb_max = max([len(g) for g in groupes])
    for g in range(len(groupes)):
        groupes[g].extend([""]* (nb_max-len(groupes[g])))

    mise_en_forme = sep.join(["{:<"+str(largeur)+"}" for largeur in largeur_max])
    merge = [
        mise_en_forme.format(*[groupe[rang] for groupe in groupes]) + "\n"
        for rang in range(nb_max)
    ]

    return merge

def print_text(groupe):
    print("".join(groupe).rstrip())


def gap(a,b) :
    """return abs(b-a)"""
    return abs(a-b)

def is_float(token) :
    """check if the token can be convert in float"""
    try :
        f = float(token)
        return True
    except :
        return False

def to_string(*args,sep="",end="\n"):
    """converti en string"""
    return sep.join(map(str, args)) + end

def get_color_name(data_colors,red,green,blue) :
    """return a color name corresponding to the r,g,b"""
    minn = float("inf")
    minn_name = "no name"
    for name, (r,g,b) in data_colors.items() :
        score = (
            gap(red,r)**2 + 
            gap(green,g)**2 + 
            gap(blue,b)**2
        )

        if score < minn :
            minn = score
            minn_name = name

    return minn_name
def load_at(name):
    dirpath, filename = os.path.split(os.path.abspath(__file__))
    with open(os.path.join(dirpath, "svg_data", name), 'r') as f:
        data = f.read()
    return eval("".join(data))

def save_at(name, data):
    dirpath, filename = os.path.split(os.path.abspath(__file__))
    with open(os.path.join(dirpath, "svg_data", name), 'w+') as f:
        f.write(repr(data))

def afficher_data(data, max_element_ligne=10):
    """affiche un grand tablea"""
    data.sort()
    for position, element in enumerate(data):
        if position%max_element_ligne ==0:
            print()
        print(element, end="\t")
    print("\nlen :",len(data))


def afficher_matrice(matrice, largeur=4, x_label="x", y_label="y"):
    afficher_elem =   "{:>"+str(largeur)+"}"
    afficher_abscisse='{:>'+str(largeur)+'}'

    print("",y_label)
    for y, ligne in enumerate(matrice[::-1]):
        y = len(matrice)-y-1
        print('{:>3} | '.format(y), end="")
        for element in ligne:
            print(afficher_elem.format(element),end="")
        print()

    print("----|", "-"*largeur*len(matrice[0]),sep="-")
    print("    | ",end="")

    for x in range(len(matrice[0])):
        print(afficher_abscisse.format(x),end="")

    print(" ",x_label)

def flatten_list(table):
    """flatten
    >>> flatten_list([[1,2,3],[4]])
    [1, 2, 3, 4]
    """
    return [p for couleur in table for p in couleur]

def tab_all(text):
    return text.replace("\n", "\n\t")

def most_frequent(List):
    return max(set(List), key = List.count)

def Mstring(matrice, largeur=1, x_label="x", y_label="y", translate=lambda x:x):
    result = [""]
    def fake_print(*args, end="\n", sep=" "):
        result[-1] += sep.join(map(str, args)) + end
        if end == "\n": result.append("")

    original = lambda x:x
    if translate !=  original:
        n = len(matrice[0])
        matrice = [
            [matrice[translate(debut)][translate(fin)]
             for fin in range(n)
            ]for debut in range(n)
        ]

    afficher_elem =   "{:>"+str(largeur)+"}"
    afficher_abscisse='{:>'+str(largeur)+'}'

    fake_print("",y_label)
    for y, ligne in enumerate(matrice[::-1]):
        y = len(matrice)-y-1
        fake_print('{:>3} | '.format(translate(y)), end="")
        for element in ligne:
            fake_print(afficher_elem.format(element),end="")
        fake_print()

    fake_print("----|", "-"*largeur*len(matrice[0]),sep="-")
    fake_print("    | ",end="")

    for x in range(len(matrice[0])):
        fake_print(afficher_abscisse.format(translate(x)),end="")

    fake_print(" ",x_label)
    return result

def afficher_matrice(matrice, largeur=4, x_label="x", y_label="y", translate=lambda x:x):
    print("".join(Mstring(matrice, largeur=largeur, x_label=x_label, y_label=y_label, translate=translate)))