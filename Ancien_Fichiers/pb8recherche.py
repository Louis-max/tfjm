import copy
import os
import operator
import itertools


data = []
with open("resultat.txt", "r+") as f:
	for line in f.readlines():
		data.append(eval(line))

def afficher_compres(synthese):
    fleche = {1:"-->", 2:"~> "}
    return (
        "[" + 
        ", ".join(sorted([str(debut) + " => " + str(fin) for (debut, fin), p in synthese if p == 1])) +
        ", ".join(sorted([str(debut) + " -> " + str(fin) for (debut, fin), p in synthese if p == 2])) +
        "]")

for raw in data[:10] :
	position, taille, objectifs, preferences = raw
	preference = preferences[0]
	print("---------------",position,"/",taille)	
	print("objectifs   :", [[c+h*3 for c,h in couleur] for couleur in objectifs])
	print("préférences :",afficher_compres(preference))

