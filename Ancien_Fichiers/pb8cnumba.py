import copy
import os
import operator
import itertools
#from numba import jit

def get_couleur(p):
    return p%3

def get_hauteur(p):
    return p//3

def get_participant(c,h):
    return h*3 + c

def decompose(participant):
    return get_couleur(participant), get_hauteur(participant)

def flatten_list(table):
    return [p for couleur in table for p in couleur]

def Leurcou(Pc, Oc):
    """renvoie la couleur qui n'est n'y celle de l'objectif n'y celle du participant"""
    s = tuple(sorted([Pc, Oc]))
    return {
        (0,1) : 2,
        (1,2) : 0,
        (0,2) : 1
    }[s]

def Synthese(liaisons):
    """renvoie la synthese des liaisons
    synthese[(debut, fin)]=min(p)
    """
    synthese, hauteur = {}, len(liaisons[0])
    for c in range(3) :
        for h in range(hauteur):
            for Cible_p, Cible_c, Cible_h in liaisons[c][h]:
                token = (h*3+c, Cible_h*3 + Cible_c)
                if token not in synthese.keys():
                    synthese[token] = Cible_p
                synthese[token] = min(synthese[token], Cible_p)
    return tuple(sorted(synthese.items()))

def compress_objectifs(objectifs):
    return tuple([
                tuple([
                    (get_couleur(objectif), get_hauteur(objectif))
                    for objectif in couleur
                ])  for couleur in objectifs
            ]) 

def rechercher_table(objectifs_compresse, show=False, debug=False):
    """
    P pour Premier
    I pour intermediaire
    O pour objectif (de P)
    Le chemin fait :
       P
    -> P (en dessous)
    -> I
    -> I (en dessous)
    -> O (au dessus)
    -> O
    """

    #compression
    NB_COULEURS = 3
    HAUTEUR_MAX = len(objectifs_compresse[0])
    OBJECTIFS = objectifs_compresse
    resultat = []

    #fonctions
    def interference(liaisons, Pc, Ph, Ic, Ih, Oc, Oh):
        """renvoie true si mettre le joueur I en intermediaire créer des interferences"""

        #liaison 1
        Pdessous = (Ph+1)%HAUTEUR_MAX
        for p, c, h in liaisons[Pc][Pdessous] :
            if p==1: return True
            if (p == 2 and c == Ic) and (h != Ih) :
                return True #les deux liaisons n'ont pas le même objectif

        #Liaison 2
        Idessous= (Ih+1)%HAUTEUR_MAX
        Odessus = (Oh-1)%HAUTEUR_MAX
        for p, c, h in liaisons[Ic][Idessous] :
            if (c == Oc and h != Odessus) :
                return True #les deux liaisons n'ont pas le même objectif

        return False

    def recursion(liaisons_original, participant):
        Pc, Ph = decompose(participant)
        if participant == HAUTEUR_MAX * NB_COULEURS - 1 :
            resultat.append(Synthese(liaisons_original))
            if show: print("\t", afficher_compres(Synthese(liaisons_original)))
            return

        Oc, Oh = OBJECTIFS[Pc][Ph]
        Ic = Leurcou(Pc, Oc)
        for Ih in range(HAUTEUR_MAX):
            if not interference(liaisons_original, Pc, Ph, Ic, Ih, Oc, Oh):  #on peut le rajouter
                liaisons = copy.deepcopy(liaisons_original)
                Pdessous = (Ph+1)%HAUTEUR_MAX
                Idessous = (Ih+1)%HAUTEUR_MAX
                Odessus  = (Oh-1)%HAUTEUR_MAX

                liaisons[Pc][Pdessous].append((1, Ic, Ih))
                liaisons[Ic][Idessous].append((2, Oc, Odessus))
                recursion(liaisons, participant+1)

    #initialisation
    liaisons_vierge = \
        tuple([
            tuple([
                [] for _ in range(HAUTEUR_MAX)
            ]) for _ in range(NB_COULEURS)
        ])
    if debug: print("objectifs :",objectifs)
    if debug: print("listes possibles :")
    recursion(liaisons_vierge, 0)
    if debug: print("fin")
    return resultat

def most_frequent(List):
    return max(set(List), key = List.count)

def mixe_hauteur(objectifs_compresse):
    """renvoie toutes les listes d'objectifs qui garde les mêmes couleurs mais change les hauteurs"""
    hauteur = len(objectifs_compresse[0])
    hauteurs = tuple(range(hauteur))
    return set([
        tuple(
            tuple((
                (joueur:=objectifs_compresse[c][tableau_translation[c][h]])[0],
                tableau_translation[joueur[0]][joueur[1]]
            )
                for h in range(hauteur)
            )   for c in range(3)
        ) 

        for tableau_translation in [
            [colonne1] + [colonne2] + [colonne3]
            for colonne1 in itertools.permutations(hauteurs, hauteur)
            for colonne2 in itertools.permutations(hauteurs, hauteur)
            for colonne3 in itertools.permutations(hauteurs, hauteur)
        ]
    ])

def load_at(name):
    dirpath, filename = os.path.split(os.path.abspath(__file__))
    with open(os.path.join(dirpath, "svg_data", name), 'r+') as f:
        data = f.read()
    return eval("".join(data))

def save_at(name, data):
    dirpath, filename = os.path.split(os.path.abspath(__file__))
    with open(os.path.join(dirpath, "svg_data", name), 'w+') as f:
        f.write(repr(data))

def main() :
    # objectifs = [[1,4,7,10],[2,5,8,11],[0,3,6,9]]
    objectifs = [[1,4,7],[2,5,8],[3,6,0]]
    n = len(objectifs[0])*3
    Cobjectifs = compress_objectifs(objectifs)
    recap = []
    pos_debut = 0
    if input("reset ? (oui/non)") == "oui" :
        print("reset !")
        with open("resultat.txt", "w+") as myfile:
            myfile.write("")
    else :
        pos_debut = load_at("pb8pos_debut")
        print("on reprend depuis",pos_debut)

    minn_taille = float("inf")
    a_faire = mixe_hauteur(Cobjectifs)
    for pos, alternative in tuple(enumerate(a_faire))[pos_debut::]:
        save_at("pb8pos_debut", pos)
        print("{:>6}/{:>6} alternative :".format(pos ,len(a_faire)), "len :",end="")
        if preference := rechercher_table(alternative):
            print(len(preference),"min",minn_taille)
            minn_taille = min(minn_taille, len(preference))
            recap.append((pos, len(preference), alternative))
            with open("resultat.txt", "a+") as myfile:
                myfile.write(repr((pos, len(preference), alternative, preference))+"\n")
        else :
            print()

    print("fin !")

main()
