import itertools
from math import factorial

def generer_colonne(n,p):
    """gènère toutes les préférence par p possible
    p est la personne qui se voit confier la ligne, il ne peux donc pas être dans le classement

    >>> [list(combi) for combi in generer_colonne(3,0)]
    [[1, 2], [2, 1]]
    """
    adversaire = list(range(n))
    del adversaire[p]   #p pas dans la liste
    return [list(colonne) for colonne in itertools.permutations(adversaire, len(adversaire))]

def generer_table(n):
    """génère une liste random pas forcément possible
    on sait qu'il y a un seul nom pareille possible par colonne
    >>> [t for t in generer_table(3)]
    [[[1, 2], [0, 2], [0, 1]], [[1, 2], [0, 2], [1, 0]], [[1, 2], [2, 0], [0, 1]], [[1, 2], [2, 0], [1, 0]], [[2, 1], [0, 2], [0, 1]], [[2, 1], [0, 2], [1, 0]], [[2, 1], [2, 0], [0, 1]], [[2, 1], [2, 0], [1, 0]]]
    """

    def generateur(n,numero_colonne):
        if numero_colonne==n-1:
            for colonne in generer_colonne(n,numero_colonne):
                yield [colonne]

        else :
            for colonne in generer_colonne(n,numero_colonne):
                for colonne_suivant in generateur(n, numero_colonne+1):
                    yield [colonne] + colonne_suivant

    for colonne in generateur(n,0):
        yield colonne 

def afficher_table(table):
    case = "{:>12}" 
    for participant in range(len(table)):
        print(case.format(participant),end="")
    print()

    for colonne in range(len(table)):
        for ligne in range(len(table[0])):
            print(case.format(table[colonne][ligne]), end="")
        print()

def resultat(table, vainqueur):
    """détermine le vainqueur, a chaque fois le plus faible est éliminé, si le tableau de pref arrive sur un -1, renvoie une erreur

    >>> riri=0; fifi=1; loulou=2
    >>> table_demo = [[fifi,loulou],[loulou,riri],[riri,fifi]]
    >>> resultat(table_demo,riri)
    1
    >>> resultat(table_demo,fifi)
    2
    >>> resultat(table_demo,loulou)    
    0
    """
    n = len(table)
    eliminé = [False for _ in range(n)]
    eliminé[vainqueur] = True
    for etape in range(n-1):
        #print("vainqueur ->",vainqueur)
        pointeur_victime = -1
        if table[vainqueur][pointeur_victime] == -1 : return -1     #si c'est -1 on renvoie une erreur

        while eliminé[table[vainqueur][pointeur_victime]]:          #pas encore éliminé
            pointeur_victime -= 1                                   #sinon on prend le prochain sur la liste
            if table[vainqueur][pointeur_victime] == -1 : return -1 #prochain qui doit être définis
        vainqueur = table[vainqueur][pointeur_victime]              #que l'on nome gagnant/éliminé
        eliminé[vainqueur] = True                                   #qui choisit la prochaine personne
    return vainqueur                                                #on à finit après n-1 coups !

def correspond(objectifs, table):
    """renvoie True si la table correspond à l'objectif"""
    for perdant, gagnant in enumerate(objectifs):
        if resultat(table, perdant) != gagnant :
            return False
    return True

def recherche_table(objectifs, stop_premier_trouvé=True):
    """attention, liste objectif commence à zero"""
    print("recherche de la table pour les obj :",objectifs)
    ctr, n = 0, len(objectifs)
    for table in generer_table(len(objectifs)) :
        ctr += 1
        if ctr%100000==0:print("{:>10} tables testées".format(ctr))
        if correspond(objectifs, table):
            print("->",table)
            print("trouvé :)")
            return 
    return 
    print("rien trouvé :'(")


def est_cyclique(table):
    """on défini par cycle chaque participant qui est éliminé en premier fait gagner le participant suivant

    >>> riri=0; fifi=1; loulou=2
    >>> table_demo = [[fifi,loulou],[loulou,riri],[riri,fifi]]
    >>> est_cyclique(table_demo)
    True
    >>> table_fausse = [[loulou,fifi],[loulou,riri],[riri,fifi]]
    >>> est_cyclique(table_fausse)
    False
    """
    for participant in range(len(table)):
        if resultat(table, participant) != (participant+1)%len(table):
            return False
    return True

def main(): 
    obj = [2,3,3,2] #0->2<->3<-1
    recherche_table(obj)
    # recherche_table(obj,stop_premier_trouvé=False)
    input("")
main()

if __name__ == '__main__':
    import doctest
    doctest.testmod()
