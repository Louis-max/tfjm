import itertools
from utils import *


class piza():
	def __init__(self, poid, duree, symbole="X"):
		self.poid = poid
		self.duree = duree
		self.symbole = symbole

def get_poid_duree(pizzas):
	for pizza in pizzas:
		yield pizza.poid, pizza.duree

def brut_force(data):
	score_min = float("inf")
	config=None
	data = tuple(get_poid_duree(pizzas))
	for pizzas in itertools.permutations():
		heure = 0
		poid_total = 0
		for poid, duree in pizzas:
			heure += duree
			poid_total += duree * poid

		if poid_total < score_min :
			score_min = poid_total
			config = pizzas
	return config, poid

def test1(pizzas):
	def get_score(pizza, pizzas):
		score = 0
		for poid, duree in get_poid_duree(pizzas):
			score += pizza.duree * poid

	score_toto = 0
	ordre = []
	while pizzas:
		min_score = float("")
		for i in range(len(pizzas)):
			select = pizzas[i]
			autres = pizzas[:i:] + pizzas[i+1::]
			sc = get_score(select, autres)
		pas finit 