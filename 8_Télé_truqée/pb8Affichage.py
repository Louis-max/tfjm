"""Pour un affichage plus complet, on simule ici des prints que l'on stocke dans un tableau et que l'on assemble avant de réélement les afficher"""
SYMBOLE = {3: " ", 2: "·", 1: "¤", "impossible": "█"}

def tab_all(text):
    """applique une tabulation"""
    return text.replace("\n", "\n\t")

def get_couleur(p):
    """
    >>> [get_couleur(i) for i in range(4)]
    [0, 1, 2, 0]
    """
    return p % 3


def get_hauteur(p):
    """
    >>> [get_hauteur(i) for i in range(4)]
    [0, 0, 0, 1]
    """
    return p // 3


def get_participant(c, h):
    """
    >>> [get_participant(*decompose(i)) for i in range(4)]
    [0, 1, 2, 3]
    """
    return h * 3 + c

def flatten_list(table):
    """flatten
    >>> flatten_list([[1,2,3],[4]])
    [1, 2, 3, 4]
    """
    return [p for couleur in table for p in couleur]

def decompose(participant):
    """
    >>> [decompose(i) for i in range(4)]
    [(0, 0), (1, 0), (2, 0), (0, 1)]
    """
    return get_couleur(participant), get_hauteur(participant)

def merge_fake_print(*groupes, sep="   |   "):
    """permet d'assembler des groupes d'affichage"""
    largeur_max = [
        max([
            len(row)
            for row in g
        ]) for g in groupes
    ]

    groupes = [[line.rstrip() for line in g] for g in groupes]

    nb_max = max([len(g) for g in groupes])
    for g in range(len(groupes)):
        groupes[g].extend([""] * (nb_max - len(groupes[g])))

    mise_en_forme = sep.join(["{:<" + str(largeur) + "}" for largeur in largeur_max])
    merge = [
        mise_en_forme.format(*[groupe[rang] for groupe in groupes]) + "\n"
        for rang in range(nb_max)
    ]

    return merge

def print_text(groupe):
    """affiche le text"""
    print("".join(groupe).rstrip())

def Mstring(matrice, largeur=1, x_label="x", y_label="y", translate=lambda x: x):
    """renvoie le text d'une matrice bien mise en forme"""
    result = [""]

    def fake_print(*args, end="\n", sep=" "):
        result[-1] += sep.join(map(str, args)) + end
        if end == "\n": result.append("")

    original = lambda x: x
    if translate != original:
        n = len(matrice[0])
        matrice = [
            [matrice[translate(debut)][translate(fin)]
             for fin in range(n)
             ] for debut in range(n)
        ]

    afficher_elem = "{:>" + str(largeur) + "}"
    afficher_abscisse = '{:>' + str(largeur) + '}'

    fake_print("", y_label)
    for y, ligne in enumerate(matrice[::-1]):
        y = len(matrice) - y - 1
        fake_print('{:>3} | '.format(translate(y)), end="")
        for element in ligne:
            fake_print(afficher_elem.format(element), end="")
        fake_print()

    fake_print("----|", "-" * largeur * len(matrice[0]), sep="-")
    fake_print("    | ", end="")

    for x in range(len(matrice[0])):
        fake_print(afficher_abscisse.format(translate(x)), end="")

    fake_print(" ", x_label)
    return result


def afficher_matrice(matrice, largeur=4, x_label="x", y_label="y", translate=lambda x: x):
    print("".join(Mstring(matrice, largeur=largeur, x_label=x_label, y_label=y_label, translate=translate)))


def matrice_cumulation(synthese, n, par_couleur=True):
    matrice = [
        [3
         for _ in range(n)
         ] for _ in range(n)
    ]
    for tableau in synthese:
        for (debut, fin), p in tableau:
            matrice[debut][fin] = min(matrice[debut][fin], p)

    for debut in range(n):
        for fin in range(n):
            if get_couleur(debut) == get_couleur(fin):
                matrice[debut][fin] = SYMBOLE["impossible"]
            else:
                matrice[debut][fin] = SYMBOLE[matrice[debut][fin]]

    return matrice

def matrice_objectif(objectifs, n):
    n = len(objectifs[0]) * 3
    matrice = [
        [
            ""
            for fin in range(n)
        ] for debut in range(n)
    ]
    for c, couleur in enumerate(objectifs):
        for h, objectif in enumerate(couleur):
            matrice[get_participant(c, h)][objectif] = SYMBOLE[1]

    for debut in range(n):
        for fin in range(n):
            if get_couleur(debut) == get_couleur(fin):
                matrice[debut][fin] = SYMBOLE["impossible"]
    return matrice

def ParCouleur(x):
    return 3 * (x % 3) + x // 3