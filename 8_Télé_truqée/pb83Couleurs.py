import copy
import itertools
SYMBOLE = {3: " ", 2: "·", 1: "¤", "impossible": "█"}

affichage_avancé = True
try:
    from pb8Affichage import *
except ImportError:
    print("je n'ai pas trouvé le fichier pb8Affichage.py est-t-il bien dans le même dossier que ce programme ? Sinon vous pouvez le copier coller ici et désactiver ce passage")
    input("(appuyez pour continuer) ")
    affichage_avancé = False

INTRO = """Nos programmes python pour la recherche concernant les 3 colorations
Pour chaque joueur nous avons sa couleur et sa hauteur (sa position parmi sa couleur)
Chaque joueur d'une couleur élimine directement le joueur de sa couleur de hauteur (h - 1)%H
On n’affiche pas ces liaisons
Les joueurs sont numérotés à partir de 0
Le joueur le plus haut est celui de hauteur 0 et le plus bas celui de hauteur H

Si besoin contacter edelrine#8886 ou louis-max.harter@protonmail.com pour + d'infos (ʘ‿ʘ)╯
"""


def get_couleur(p):
    """
    >>> [get_couleur(i) for i in range(4)]
    [0, 1, 2, 0]
    """
    return p % 3


def get_hauteur(p):
    """
    >>> [get_hauteur(i) for i in range(4)]
    [0, 0, 0, 1]
    """
    return p // 3


def get_participant(c, h):
    """
    >>> [get_participant(*decompose(i)) for i in range(4)]
    [0, 1, 2, 3]
    """
    return h * 3 + c

def flatten_list(table):
    """flatten
    >>> flatten_list([[1,2,3],[4]])
    [1, 2, 3, 4]
    """
    return [p for couleur in table for p in couleur]

def decompose(participant):
    """
    >>> [decompose(i) for i in range(4)]
    [(0, 0), (1, 0), (2, 0), (0, 1)]
    """
    return get_couleur(participant), get_hauteur(participant)

def couleur_complémentaire(Pc, Oc):
    """renvoie la couleur qui n'est n'y celle de l'objectif n'y celle du participant"""
    s = tuple(sorted([Pc, Oc]))
    return {
        (0, 1): 2,
        (1, 2): 0,
        (0, 2): 1
    }[s]


def Synthese(liaisons):
    """renvoie la synthese des liaisons, elle ne garde que les liaisons de poids minimum
    synthese[(debut, fin)]=min(p)
    """
    synthese, hauteur = {}, len(liaisons[0])
    for c in range(3):
        for h in range(hauteur):
            for Cible_p, Cible_c, Cible_h in liaisons[c][h]:
                token = (h * 3 + c, Cible_h * 3 + Cible_c)
                if token not in synthese.keys():
                    synthese[token] = Cible_p
                synthese[token] = min(synthese[token], Cible_p)
    return tuple(sorted(synthese.items()))


def afficher_compres(synthese):
    fleche = {1: "-->", 2: "~> "}
    return (
            "[" +
            ", ".join(sorted([str(debut) + " => " + str(fin) for (debut, fin), p in synthese if p == 1])) +
            ", ".join(sorted([str(debut) + " -> " + str(fin) for (debut, fin), p in synthese if p == 2])) +
            "]")

def compress_objectifs(objectifs):
    """covnertie l'index des objectifs en couple hauteur couleurs"""
    return tuple([
        tuple([
            (get_couleur(objectif), get_hauteur(objectif))
            for objectif in couleur
        ]) for couleur in objectifs
    ])

def rechercher_table(objectifs_compresse, show=False, debug=False):
    """On cherche une table
    P pour Premier joueur éliminé
    I pour intermediaire
    O pour objectif (de P)
    Le chemin fait :
    P -> P (en dessous) -> I -> I (en dessous) -> O (au dessus) -> O
    """

    # compression
    NB_COULEURS = 3
    HAUTEUR_MAX = len(objectifs_compresse[0])
    OBJECTIFS = objectifs_compresse
    resultat = []

    def est_possible(liaisons, Pc, Ph, Ic, Ih, Oc, Oh):
        """renvoie true si mettre le joueur I en intermediaire créer une situation impossible"""

        # liaison 1
        Pdessous = (Ph + 1) % HAUTEUR_MAX
        for p, c, h in liaisons[Pc][Pdessous]:
            if p == 1: return True
            if (p == 2 and c == Ic) and (h != Ih):
                return True  # les deux liaisons n'ont pas le même objectif

        # Liaison 2
        Idessous = (Ih + 1) % HAUTEUR_MAX
        Odessus = (Oh - 1) % HAUTEUR_MAX
        for p, c, h in liaisons[Ic][Idessous]:
            if (c == Oc and h != Odessus):
                return False  # les deux liaisons n'ont pas le même objectif

        return True

    def recursion(liaisons_original, participant):
        """on test toutes les combinaisons, DFS"""
        Pc, Ph = decompose(participant)
        if participant == HAUTEUR_MAX * NB_COULEURS - 1:
            resultat.append(Synthese(liaisons_original))
            if show: print("\t", afficher_compres(Synthese(liaisons_original)))
            return

        Oc, Oh = OBJECTIFS[Pc][Ph]
        Ic = couleur_complémentaire(Pc, Oc)
        for Ih in range(HAUTEUR_MAX):
            if est_possible(liaisons_original, Pc, Ph, Ic, Ih, Oc, Oh):  # on peut le rajouter
                liaisons = copy.deepcopy(liaisons_original)
                Pdessous = (Ph + 1) % HAUTEUR_MAX
                Idessous = (Ih + 1) % HAUTEUR_MAX
                Odessus = (Oh - 1) % HAUTEUR_MAX

                liaisons[Pc][Pdessous].append((1, Ic, Ih))
                liaisons[Ic][Idessous].append((2, Oc, Odessus))
                recursion(liaisons, participant + 1)

    # initialisation
    liaisons_vierge = \
        tuple([
            tuple([
                [] for _ in range(HAUTEUR_MAX)
            ]) for _ in range(NB_COULEURS)
        ])
    if debug: print("objectifs :", objectifs)
    if debug: print("listes possibles :")
    recursion(liaisons_vierge, 0)
    if debug: print("fin")
    return resultat

def menu():
    print(INTRO)
    print("\n\n")
    OUI = ("oui","o","y","yes") 

    exemples =(
        ([[1, 5, 4], [2, 8, 6], [0, 3, 7]], "2, 3 et 4 cycle."),
        ([[1, 5, 8], [2, 0, 3], [4, 6, 7]], "4 et 5 cycles"),
        ([[1, 4, 7], [2, 5, 8], [0, 3, 6]], "3,3,3 cycles"),
        ([[1,4,7,10],[2,5,8,11],[3,6,9,0]], "12 cycles, une seule solution"),
        ([[1,4,8],[2,5,0],[3,6,7]], "9 cycles, arrangé à la fin"),
        ([[1,4,8],[2,5,0],[3,6,7]], "9 cycles, en inversant le 7 et le 8"),
        ([[8,2,5],[0,3,6],[1,4,7]], "9 cycles dans l'autre sens -> résultat normale"),
        ([[1,4,7,10],[2,5,8,11],[0,3,6,9]], "3,3,3,3 cycles"),
        ([[1,4,7,11],[2,5,8,9],[0,3,6,10]], "3,3,3,3 cycles (dont un dans l'autre sens)")
    )

    def convertisseur(text):
        try :
            return eval(text)
        except :
            return None

    while True : 
        commande = input("commande : ").strip()
        objectif = None

        if commande in ("list", "l", "choix", "d", "data", "ls"):
            for objectifs, commentaire in exemples :
                print(objectifs, "\t", commentaire)

        elif commande in ("q", "leave", "exit"):
            print("A+")
            exit(0)

        elif isinstance(convertisseur(commande), list):
            objectif = isinstance(convertisseur(commande), list)

        elif isinstance(convertisseur(commande), int):
            index =  convertisseur(commande)
            objectif = exemples[index][0]
            print("label :", exemples[index][1])

        else :
            print("commande inconnue ! '",commande,"'",sep="")
            print("ls pour afficher la liste des 3 couleurs que nous avons étudiées")
            print("q  pour quitter")
            print("un entier pour lancer une recherche sur le tableau correspondant")
            print("un tableau de préférence pour lancer une recherche sur celui-ci")

        if objectif != None :
            objectifs = objectif #bien crade
            h = len(objectifs[0])
            n = h * 3
            #on compresse les objectifs pour ne garder que leurs couleurs/hauteur
            Cobjectifs = tuple([
                    tuple([
                        (get_couleur(objectif), get_hauteur(objectif))
                        for objectif in couleur
                    ])  for couleur in objectifs
                ])
            synthese = rechercher_table(Cobjectifs)
            print("objectifs:", objectifs, "\tNombre de solutions :", len(synthese))
            if input("garder un seul tableau de préférence : ").strip() in OUI :
                synthese = [synthese[0]] if synthese else []


            if affichage_avancé :
                Mobjectifs = matrice_objectif(objectifs, n)
                Msynthese = matrice_cumulation(synthese, n)
                if len(synthese) <= 2:
                    for s in synthese: print("synthèse :", s)

                Groupe1 = merge_fake_print(["matrice d'objectif :\n"] + Mstring(Mobjectifs, translate=ParCouleur),
                                           ["matrice liens nécéssaires:\n"] + Mstring(Msynthese, translate=ParCouleur))
                Groupe2 = merge_fake_print(["matrice d'objectif :\n"] + Mstring(Mobjectifs),
                                           ["matrice liens nécéssaires:\n"] + Mstring(Msynthese))
                print_text(merge_fake_print(Groupe1, Groupe2, sep="   ||   "))

            else :
                for index, s in enumerate(synthese) :
                    print("{:>4} : ".format(index), s)
                print("\n")

#désactiver cette ligne pour ne pas passer par le menu
menu()


#pour ceux qui préfèrent passer par un editeur de code
from pb8Affichage import *

def main():
    for objectifs, label in [
        ([[1, 5, 4], [2, 8, 6], [0, 3, 7]], "2, 3 et 4 cycle."),
        ([[1, 5, 8], [2, 0, 3], [4, 6, 7]], "4 et 5 cycles"),
        ([[1, 4, 7], [2, 5, 8], [0, 3, 6]], "3,3,3 cycles"),
        # ([[1,4,7,10],[2,5,8,11],[3,6,9,0]], "12 cycles, une seule solution"),
        # ([[1,4,8],[2,5,0],[3,6,7]], "9 cycles, arrangé à la fin"),
        # ([[1,4,8],[2,5,0],[3,6,7]], "9 cycles, en inversant le 7 et le 8"),
        # ([[8,2,5],[0,3,6],[1,4,7]], "9 cycles dans l'autre sens -> résultat normale"),
        # ([[1,4,7,10],[2,5,8,11],[0,3,6,9]], "3,3,3,3 cycles"),
        # ([[1,4,7,11],[2,5,8,9],[0,3,6,10]], "3,3,3,3 cycles (dont un dans l'autre sens)"),
        ("fin", "fin")
    ]:
        if objectifs == "fin": continue
        h = len(objectifs[0])
        n = h * 3
        #on compresse les objectifs pour ne garder que leurs couleurs/hauteur
        Cobjectifs = tuple([
                tuple([
                    (get_couleur(objectif), get_hauteur(objectif))
                    for objectif in couleur
                ]) for couleur in objectifs
            ])
        Mobjectifs = matrice_objectif(objectifs, n)

        synthese = rechercher_table(Cobjectifs)
        print("objectifs:", objectifs, "\t(", len(synthese), ") :", label)

        synthese = [synthese[0]] if synthese else []
        Msynthese = matrice_cumulation(synthese, n)
        if len(synthese) <= 2:
            for s in synthese: print("->", s)

        Groupe1 = merge_fake_print(["matrice d'objectif :\n"] + Mstring(Mobjectifs, translate=ParCouleur),
                                   ["matrice liens nécéssaires:\n"] + Mstring(Msynthese, translate=ParCouleur))
        Groupe2 = merge_fake_print(["matrice d'objectif :\n"] + Mstring(Mobjectifs),
                                   ["matrice liens nécéssaires:\n"] + Mstring(Msynthese))
        print_text(merge_fake_print(Groupe1, Groupe2, sep="   ||   "))

    if False:
        if synthese:
            for k, s in enumerate(synthese):
                print("{:>4} : ".format(k), afficher_compres(s))
            tikz_descendance(synthese)
        else:
            print("rien trouvé :'(")


main()

if __name__ == '__main__':
    import doctest
    doctest.testmod()
